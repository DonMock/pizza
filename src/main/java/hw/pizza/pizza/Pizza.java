package hw.pizza.pizza;

import java.io.Serializable;
import java.util.Objects;

class Pizza implements Serializable {

    private String name;
    private String size;

    Pizza(String name, String size) {
        this.name = name;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pizza pizza = (Pizza) o;
        return Objects.equals(name, pizza.name) &&
                Objects.equals(size, pizza.size);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, size);
    }
}
