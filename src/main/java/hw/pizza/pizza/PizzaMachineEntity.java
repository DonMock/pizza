package hw.pizza.pizza;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import javax.persistence.*;

import hw.pizza.product.Product;

import static com.google.common.base.Preconditions.checkState;

@Entity
class PizzaMachineEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "COOK_COUNT")
    private Integer cookCount;

    @Column(name = "LAST_CLEAN")
    private Integer lastClean;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "MACHINE_PRODUCTS")
    @Column(name = "PRODUCTS")
    private Map<Product, Integer> products = new HashMap<>();

    public Long getId() {
        return id;
    }

    private void increaseCookCount() {
        cookCount++;
    }

    void clean() {
        lastClean = cookCount;
    }

    boolean isClean(int cooksBeforeClean) {
        return cookCount - lastClean >= cooksBeforeClean;
    }

    boolean canCook(Map<Product, Integer> requiredProducts) {
        return requiredProducts.entrySet().stream()
                .allMatch(this::isEnoughInStorage);
    }

    private boolean isEnoughInStorage(Map.Entry<Product, Integer> entity) {
        return products.containsKey(entity.getKey())
                && products.get(entity.getKey()) >= entity.getValue();
    }

    void cook(Map<Product, Integer> requiredProducts) {
        checkState(canCook(requiredProducts), "not enough products");
        increaseCookCount();
        requiredProducts.forEach(reduceProduct());
    }

    private BiConsumer<Product, Integer> reduceProduct() {
        return (key, value) -> products.replace(key, products.get(key) - value);
    }
}
