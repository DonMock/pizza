package hw.pizza.pizza;

import static com.google.common.base.Preconditions.*;
import static java.util.Optional.*;

import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import hw.pizza.product.Product;
import hw.pizza.recipe.Recipe;
import hw.pizza.recipe.Size;

class PizzaMachine {

    @Autowired
    private PizzaMachineRepository pizzaMachineRepository;

    @Value("${machine.cooks-before-clean:20}")
    private int cooksBeforeClean;

    private Long id;

    public PizzaMachine(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    PizzaMachineEntity get() {
        return pizzaMachineRepository.getOne(id);
    }

    @Transactional
    Optional<Pizza> make(Recipe recipe, Size size) {
        PizzaMachineEntity pizzaMachineEntity = pizzaMachineRepository.findById(id).get();
        checkState(!pizzaMachineEntity.isClean(cooksBeforeClean),
                "Machine id:" + pizzaMachineEntity.getId() + " needs to be cleaned up");
        return cook(recipe, size, pizzaMachineEntity);
    }

    boolean canMake(Recipe recipe, Size size){
        PizzaMachineEntity pizzaMachineEntity = pizzaMachineRepository.findById(id).get();
        return pizzaMachineEntity.canCook(recipe.getProductAmounts(size));
    }

    private Optional<Pizza> cook(Recipe recipe, Size size, PizzaMachineEntity pizzaMachineEntity) {
        Map<Product, Integer> requiredAmounts = recipe.getProductAmounts(size);
        if (!pizzaMachineEntity.canCook(requiredAmounts)) {
            return empty();
        }
        pizzaMachineEntity.cook(requiredAmounts);
        pizzaMachineRepository.save(pizzaMachineEntity);
        return of(new Pizza(recipe.getName(), size.toString()));
    }


}
