package hw.pizza.pizza;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/machine")
public class PizzaMachineResource {

    private final PizzaMachineService pizzaMachineService;

    public PizzaMachineResource(PizzaMachineService pizzaMachineService) {
        this.pizzaMachineService = pizzaMachineService;
    }

    @GetMapping
    public List<PizzaMachine> list(){
        return pizzaMachineService.list();
    }
    
    @GetMapping("/{id}")
    public PizzaMachine get(@PathVariable Long id){
        return pizzaMachineService.get(id);
    }
     
    @GetMapping("/{id}/cook/{recipeId}")
    public Pizza make(@PathVariable Long id, @PathVariable Long recipeId, @RequestParam String size){
        return pizzaMachineService.make(id, recipeId, size);
    }

    @GetMapping("/{id}/available")
    public List<String> make(@PathVariable Long id, @RequestParam String size){
        return pizzaMachineService.getAvailableRecipies(id, size);
    }
}
