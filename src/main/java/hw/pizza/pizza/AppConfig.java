package hw.pizza.pizza;

import java.util.function.Function;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
class AppConfig {

    @Bean
    Function<Long, PizzaMachine> pizzaMachineFactory() {
        return this::prototypeBeanWithParam;
    }

    @Bean
    @Scope(value = "prototype")
    protected PizzaMachine prototypeBeanWithParam(Long id) {
        return new PizzaMachine(id);
    }

}
