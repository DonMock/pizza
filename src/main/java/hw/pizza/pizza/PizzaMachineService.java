package hw.pizza.pizza;

import static java.util.stream.Collectors.*;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;

import hw.pizza.recipe.Recipe;
import org.springframework.stereotype.Service;

import hw.pizza.recipe.RecipeService;
import hw.pizza.recipe.Size;

@Service
class PizzaMachineService {

    private final Function<Long, PizzaMachine> pizzaMachineFactory;

    private final PizzaMachineRepository repository;

    private final RecipeService recipeService;

    private List<PizzaMachine> pizzaMachines;

    PizzaMachineService(
            Function<Long, PizzaMachine> pizzaMachineFactory,
            PizzaMachineRepository repository, RecipeService recipeService) {
        this.pizzaMachineFactory = pizzaMachineFactory;
        this.repository = repository;
        this.recipeService = recipeService;
    }

    @PostConstruct
    private void init() {
        pizzaMachines = repository.findAll().stream()
                .map(machine -> pizzaMachineFactory.apply(machine.getId()))
                .collect(toList());
    }

    List<PizzaMachine> list() {
        return pizzaMachines;
    }

    PizzaMachine get(Long id) {
        return pizzaMachines.stream()
                .filter(pizzaMachine -> pizzaMachine.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("no such service"));
    }

    Pizza make(Long pizzaMachineId, Long recipeId, String size) {
        return get(pizzaMachineId).make(recipeService.get(recipeId), Size.fromString(size))
                .orElseThrow(() -> new RuntimeException("cant make pizza, not enough products in storage"));
    }

    List<String> getAvailableRecipies(Long id, String prefSize) {
        Size size = Size.fromString(prefSize);
        return recipeService.list().stream()
                .filter(recipe -> get(id).canMake(recipe, size))
                .map(Recipe::getName)
                .collect(Collectors.toList());
    }
}
