package hw.pizza.pizza;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PizzaMachineRepository extends JpaRepository<PizzaMachineEntity, Long> {
    

}
