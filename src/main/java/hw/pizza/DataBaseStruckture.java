package hw.pizza;

import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/database")
public class DataBaseStruckture {

    private final EntityManager manager;

    public DataBaseStruckture(EntityManager manager) {
        this.manager = manager;
    }

    @GetMapping
    public void findAll() {
        nativeQuery(manager, "SHOW TABLES");
        nativeQuery(manager, "SHOW COLUMNS from RECIPE");
        nativeQuery(manager, "SHOW COLUMNS from RECIPE_PRODUCTS");
        nativeQuery(manager, "SHOW COLUMNS from PIZZA_MACHINE_ENTITY");
        nativeQuery(manager, "SHOW COLUMNS from MACHINE_PRODUCTS");
    }

    public static void nativeQuery(EntityManager em, String s) {
        System.out.printf("'%s'%n", s);
        Query query = em.createNativeQuery(s);
        List list = query.getResultList();
        for (Object o : list) {
            if (o instanceof Object[]) {
                System.out.println(Arrays.toString((Object[]) o));
            } else {
                System.out.println(o);
            }
        }
    }

}
