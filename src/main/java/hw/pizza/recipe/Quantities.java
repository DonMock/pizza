package hw.pizza.recipe;

import javax.persistence.Embeddable;

@Embeddable
class Quantities {

    private int quantityLarge;

    private int quantityMediun;

    private int quantitySmall;

    public Quantities(int quantityLarge, int quantityMediun, int quantitySmall) {
        this.quantityLarge = quantityLarge;
        this.quantityMediun = quantityMediun;
        this.quantitySmall = quantitySmall;
    }

    public Quantities() {
    }

    int getQuantity(Size size) {
        switch (size) {
            case LARGE:
                return quantityLarge;
            case MEDIUM:
                return quantityMediun;
            case SMALL:
                return quantitySmall;
            default:
                return 0;
        }
    }

}
