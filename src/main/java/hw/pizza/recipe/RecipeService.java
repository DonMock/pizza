package hw.pizza.recipe;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class RecipeService {

    private final RecipeRepository recipeRepository;

    public RecipeService(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    public List<Recipe> list() {
        return recipeRepository.findAll();
    }

    public Recipe get(Long id) {
        return recipeRepository.getOne(id);
    }

}
