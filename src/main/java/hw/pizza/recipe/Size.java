package hw.pizza.recipe;

public enum Size {
    LARGE ("large"),
    MEDIUM ("medium"),
    SMALL ("small");
    
    
    private final String size;

    Size(String size) {
        this.size = size;
    }
    public String getSize() {
        return this.size;
    }

    public static Size fromString(String text) {
        for (Size b : Size.values()) {
            if (b.size.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
    
}
