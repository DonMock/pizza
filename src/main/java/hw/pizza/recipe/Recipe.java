package hw.pizza.recipe;

import static com.google.common.base.Preconditions.*;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import hw.pizza.product.Product;
import lombok.Data;

@Data
@Entity
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @ElementCollection
    @CollectionTable(name = "RECIPE_PRODUCTS")
    @Column(name = "PRODUCTS")
    private Map<Product, Quantities> products = new HashMap<>();

    public Recipe() {
    }

    public Recipe(String name) {
        this.name = name;
    }

    public Recipe(String name, Map<Product, Quantities> ingredients) {
        this.name = name;
        this.products = ingredients;
    }

    public void addIngredient(Product product, Quantities quantities) {
        checkState(!products.containsKey(product), "Product is already in recipe");
        products.put(product, quantities);
    }

    public Map<Product, Integer> getProductAmounts(Size size) {
        return products.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getQuantity(size)));
    }

    public String getName() {
        return name;
    }
}
