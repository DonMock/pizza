INSERT INTO PRODUCT (id, name) VALUES 
  ('1', 'dough'),
  ('2', 'cheese'),
  ('3', 'tomato sauce'),
  ('4', 'sausage'),
  ('5', 'salami');

INSERT INTO RECIPE (id, name) VALUES 
  ('1', 'margarita'),
  ('2', 'kids'),
  ('3', 'salsa');

INSERT INTO RECIPE_PRODUCTS (RECIPE_ID, QUANTITY_SMALL, QUANTITY_MEDIUN, QUANTITY_LARGE, PRODUCTS_KEY) VALUES
  (1, '1', '2', '3', 1),
  (1, '2', '3', '4', 2),
  (1, '2', '4', '6', 3),
  (2, '1', '2', '3', 1),
  (2, '2', '3', '4', 2),
  (2, '2', '4', '6', 3),
  (2, '1', '2', '3', 4),
  (3, '1', '2', '3', 1),
  (3, '2', '3', '4', 2),
  (3, '2', '4', '6', 3),
  (3, '1', '2', '3', 5);
  
INSERT INTO PIZZA_MACHINE_ENTITY (id, COOK_COUNT, LAST_CLEAN) VALUES 
  ('1', '0', '-1'),
  ('2', '0', '-1'),
  ('3', '0', '-1');


INSERT INTO MACHINE_PRODUCTS (PIZZA_MACHINE_ENTITY_ID, PRODUCTS, PRODUCTS_KEY) VALUES
  (1, '100', 1),
  (1, '100', 2),
  (1, '100', 3),
  (1, '100', 4),
  (1, '100', 5),
  (2, '200', 1),
  (2, '200', 2),
  (2, '200', 3),
  (2, '200', 4),
  (2, '200', 5),
  (3, '20', 1),
  (3, '20', 2),
  (3, '20', 3),
  (3, '20', 4),
  (3, '20', 5);